#!/usr/bin/env bash

FILE=${FILE:-"docker-compose.yaml"}

rm -rf logs
mkdir -p logs
docker compose -f "$FILE" config --services | while read -r svc; do docker compose -f "$FILE" logs --no-color --no-log-prefix --since 24h "$svc" >"logs/$svc.txt"; done
echo -n "" >"logs/images.txt"
docker compose -f "$FILE" config --images | while read -r img; do docker image inspect "$img" >>"logs/images.txt"; done
docker compose -f "$FILE" config --resolve-image-digests >"logs/docker-compose.yaml"

zip -r "logs-$(date +"%Y-%m-%d-%H-%M-%S").zip" logs
