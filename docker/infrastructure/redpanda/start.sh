#!/bin/sh

/usr/local/bin/supervisord -d

/usr/bin/rpk redpanda config set redpanda.auto_create_topics_enabled true

exec /usr/bin/rpk redpanda start \
    --smp "1" \
    --reserve-memory 0M \
    --overprovisioned \
    --node-id "0" \
    --kafka-addr PLAINTEXT://0.0.0.0:9092,OUTSIDE://0.0.0.0:29092 \
    --advertise-kafka-addr "PLAINTEXT://kafka:9092,OUTSIDE://${DUT_IP:-192.168.178.80}:29092"
